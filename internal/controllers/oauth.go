package controllers

import (
	"net/http"
	"oauth/internal/usecases"
	"oauth/version"

	"github.com/gin-gonic/gin"
)

type OauthController struct {
	router  *gin.RouterGroup
	useCase usecases.OuathUsecaseImply
}

func NewOauthController(router *gin.RouterGroup, useCase usecases.OuathUsecaseImply) *OauthController {
	return &OauthController{
		router:  router,
		useCase: useCase,
	}
}

// InitRoutes
func (oauth *OauthController) InitRoutes() {

	oauth.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, oauth, "HealthHandler")
	})

	oauth.router.GET("/:version", func(ctx *gin.Context) {
		version.RenderHandler(ctx, oauth, "OauthHandler")
	})

}

// HealthHandler
func (oauth *OauthController) HealthHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with base version",
	})
}

func (oauth *OauthController) OauthHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "oauth",
	})
}
