package usecases

import (
	"oauth/internal/repo"
)

type OauthUseCase struct {
	useCase repo.OauthRepoImply
}

type OuathUsecaseImply interface {
	GetOauthCredentials()
}

func NewOauthUseCase(oauth repo.OauthRepoImply) OuathUsecaseImply {
	return &OauthUseCase{
		useCase: oauth,
	}
}

func (oauth *OauthUseCase) GetOauthCredentials() {

}
