package consts

const (
	DatabaseType     = "postgres"
	AppName          = "oauth"
	AcceptedVersions = "v1"
)

const (
	ContextAcceptedVersions       = "Accept-Version"
	ContextSystemAcceptedVersions = "System-Accept-Versions"
	ContextAcceptedVersionIndex   = "Accepted-Version-index"
)
