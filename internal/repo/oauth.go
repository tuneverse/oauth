package repo

import (
	"database/sql"
)

type OauthRepo struct {
	repo *sql.DB
}

type OauthRepoImply interface {
	GetOauthCredentials()
}

func NewOauthRepo(repo *sql.DB) OauthRepoImply {
	return &OauthRepo{
		repo: repo,
	}
}

func (oauth *OauthRepo) GetOauthCredentials() {

}
